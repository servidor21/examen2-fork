$(document).ready(function () {
    var loadStudyList = function () {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function () {
            if (xhttp.readyState == 4 && xhttp.status == 200) {
                document.getElementById("studySelect").innerHTML = xhttp.responseText;
            }
        };
        xhttp.open("POST", 'http://localhost/proyectotema7/es/study/ajaxStudyList', true);
        xhttp.setRequestHeader("Content-type","application/x-www-form-urlencoded");
        xhttp.send("idLevel=" + document.getElementById("levelSelect").value);
    }

    var loadStudyList2 = function () {
        $.post('http://localhost/proyectotema7/es/study/ajaxStudyList', 'idLevel=' + $("#levelSelect").val(), function (data) {
            $("#studySelect2").html('');
            $("#studySelect2").html(data);            
        }, 'html');

    };

    var loadStudyList3 = function () {
        $.post('http://localhost/proyectotema7/es/study/ajaxStudyList2', 'idLevel=' + $("#levelSelect").val(), function (data) {
//            $("#studySelect3").html('');
//            $("#studySelect3").html(data);
            
            $("#studySelect3").html('');
            
            for(var i = 0; i < data.length; i++){
                $("#studySelect3").append('<option value="' + data[i].id + '">' + data[i].nombre + '</option>');
            }
        }, 'json');

    };


    $("#levelSelect").change(function () {
        loadStudyList();
        loadStudyList2();
        loadStudyList3();
    })
});



